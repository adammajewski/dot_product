/*

how to compute dot product :
https://en.wikipedia.org/wiki/Dot_product

of two mpc_t numbers
http://www.multiprecision.org/

Adam Majewski           




--------------- comment ----------------
> It works, but have I done it properly ?
it depends on what is your objective:

* do you want a result with fixed precision only (double) or arbitrary
  precision (mpfr_t for example) ?

* do you want the intermediate variables in your code to have fixed
  precision, or same precision than the inputs, or do you want the output
  to be correctly rounded ?

Note that mpc_mul, when given a+i*c and d+i*b, will compute a*b + c*d
correctly rounded in the imaginary part of the result.

Paul Zimmermann
____________________





gcc d.c  -lmpc -lmpfr -lgmp -Wall 
./a.out

a = (1.0000000000000000000000000000000000000000000000000000000000000 2.0000000000000000000000000000000000000000000000000000000000000)  
b = (1.0000000000000000000000000000000000000000000000000000000000000 3.0000000000000000000000000000000000000000000000000000000000000)  
a.b = 7.000000 



*/


/*#include <stdio.h>*/
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>

// arbitrary precision library  settings
int base=10;
mp_prec_t mp_prec = 200;
mpfr_prec_t mpfr_prec =200;
mpc_rnd_t mp_rnd =  MPC_RNDNN; // mpc Rounding Mode
mpfr_rnd_t mpfr_rnd = MPFR_RNDN ; // mpfr Rounding Mode

int inex; // return value of the function ( not the result of computation )




// https://en.wikipedia.org/wiki/Dot_product
double  mpc_dot (mpc_t op1, mpc_t op2, mpfr_prec_t mpfr_prec)
{
  double result =0.0;
  mpfr_t mul1, mul2 ; // mul1 = re(op1)*re(op2) ; mul2 = im(op1)*im(op2) 
  mpfr_t s ; // s = mul1 + mul2

  mpfr_inits2(mpfr_prec, mul1, mul2, s, NULL);
  

  //
  mpfr_mul (mul1,  mpc_realref( op1), mpc_realref( op2), mpfr_rnd ); // mul1 = re(op1)*re(op2) 
  mpfr_mul (mul2,  mpc_imagref( op1), mpc_imagref( op2), mpfr_rnd); //   mul2 = im(op1)*im(op2) 
  // s = mul1 + mul2
  mpfr_add ( s, mul1, mul2,  mpfr_rnd);
  result =  mpfr_get_d (s, mpfr_rnd);

  
  //
  mpfr_clears(mul1, mul2, s, NULL);
  
 return result;
}







int main()

{

   double dotproduct ;
  // declare
  mpc_t a;
  mpc_t b; 
  // init 
  mpc_init2(a, mp_prec);
  mpc_init2(b, mp_prec);
  // set
  inex = mpc_set_d_d (a, 1.0, 2.0 , mp_rnd); // create c value from 2 double values 
  printf ("a = "); mpc_out_str (stdout, 10, 0, a, mp_rnd); printf ("  \n"); 
  inex = mpc_set_d_d (b, 1.0, 3.0 , mp_rnd); // create c value from 2 double values 
  printf ("b = "); mpc_out_str (stdout, 10, 0, b, mp_rnd); printf ("  \n"); 
 //
  dotproduct = mpc_dot(a,b, mpfr_prec);
  printf ("a.b = %f \n", dotproduct); 




 // clear
 mpc_clear(a);
 mpc_clear(b);

  

 return 0;
}
